import React, { useContext } from 'react'
import classNames from 'classnames'
import ReactDOM from 'react-dom'
import { ModalContext } from '../store/modal/ModalContext';

const modalRoot = document.getElementsByTagName('body')[0];

const Modal = (props, ref) => {

    // let [show, setShow] = useState(false);

    let id = props.id || Math.random().toString(36).substr(2, 9)
    let { show, hideModal } = useContext(ModalContext)

    let classes = classNames({
        'modal': true,
        'modal--show': props.show || show.indexOf(id) > -1,

        'modal--main': (props.color === 'main'),
        'modal--primary': (props.color === 'primary'),
        'modal--transparent': (props.color === 'transparent')
    }, props.className || []);

    let styles = props.style ? props.style : {};

    let attr = {
        id: `modal-${props.id}`,
        className: classes,
        style: {...styles}
    };

    let overlay = React.createElement('div', {className: 'modal__overlay', key: 0, onClick: () => hideModal(id)})
    
    let dialogHeaderClose = React.createElement('span', {className: 'modal__dialog-close fal fa-times', key: 1, onClick: () => hideModal(id)})
    let dialogHeaderTitle = props.title ? React.createElement('span', {className: 'modal__dialog-title', key: 0}, props.title) : ''
    let dialogHeader = React.createElement('div', {className: 'modal__dialog-header', key: 0}, [dialogHeaderTitle, dialogHeaderClose])
    let dialogBody = React.createElement('div', {className: 'modal__dialog-body', key: 1}, props.children)
    let dialog = React.createElement('div', {className: 'modal__dialog', key: 1}, [dialogHeader, dialogBody])

    let el = React.createElement( 'div', attr, [overlay, dialog]);

    return ReactDOM.createPortal(el, modalRoot);
}

export default React.forwardRef(Modal)