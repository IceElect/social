import { useParams } from "react-router"

interface ParamTypes {
    id: string
}

const ProfilePage = (props) => {

    const { id } = useParams<ParamTypes>();

    return (
        <div>{id}</div>
    )
}

export default ProfilePage