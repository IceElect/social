import { STORAGE_KEYS } from './constants';

const getIsLoggedIn = () => localStorage.getItem(STORAGE_KEYS.TOKEN) || '';

export default getIsLoggedIn;