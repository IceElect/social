export const STORAGE_KEYS = Object.freeze({
    TOKEN: 'token',
    IS_LOGGED_IN: 'isLoggedIn',
});