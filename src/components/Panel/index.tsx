import { useContext } from "react"
import { AppContext } from "../../context/App/AppContext"
import Chat from "./Chat"
import Friends from "./Friends"
import Notify from "./Notify"
import Settings from "./Settings"

const Panel = (props) => {

    const { tabbar: selected } = useContext(AppContext)

    const panels = {
        friends: Friends,
        chat: Chat,
        notify: Notify,
        settings: Settings
    }

    const CurrentPanel = panels[selected] || panels.friends

    return (
        <div className="panel">
            <div className="panel__inner">
                <CurrentPanel />
            </div>
        </div>
    )
}

export default Panel