import { NavLink } from "react-router-dom"
import classnames from 'classnames'
import route from "../../hoc/route"
import menu from "./_menu"
import { useContext } from "react"
import { AppContext } from "../../context/App/AppContext"
import { UserContext } from "../../context/User/UserContext"

const Tabbar = (props) => {

    const {logout} = useContext(UserContext)
    const {tabbar: selected, setTabbar} = useContext(AppContext)

    return (
        <div className="tabbar">
            <div className="tabbar__inner">
                <div className="tabbar__logo">
                    
                </div>
                <div className="tabbar__menu">
                    <NavLink to={route('profile', 1)} className="tabbar__menu-item">
                        <i className={`tabbar__menu-item-icon fal fa-fw fa-home`}></i>
                    </NavLink>
                    {menu.map((item, k) => {
                        if(item.type === 'spacer') 
                            return <div className="spacer" key={k}></div>

                        let active = (selected === item.type)

                        return (
                            <span className={classnames("tabbar__menu-item", {active})} key={k} onClick={() => setTabbar(item.type)}>
                                <i className={`tabbar__menu-item-icon fal fa-fw fa-${item.icon}`}></i>
                            </span>
                        )
                    })}
                    <span className="tabbar__menu-item" onClick={() => logout()}>
                        <i className={`tabbar__menu-item-icon fal fa-fw fa-sign-out`}></i>
                    </span>
                </div>
            </div>
        </div>
    )
}

export default Tabbar