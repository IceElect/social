export default [
    {
        type: 'friends',
        icon: 'user',
        title: 'Друзья'
    },
    {
        type: 'chat',
        icon: 'envelope',
        title: 'Сообщения'
    },
    {
        type: 'notify',
        icon: 'bell',
        title: 'Уведомления'
    },
    {
        type: 'spacer'
    },
    // {
    //     type: 'settings',
    //     icon: 'cog',
    //     title: 'Настройки'
    // }
]