import { useContext } from "react"
import { Link } from "react-router-dom"
import { useForm } from 'react-hook-form';
import { UserContext } from "../../context/User/UserContext"
import route from "../../hoc/route"
import Button from "../../UI/Button"
import Control from "../../UI/Control"

const LoginForm = () => {

    const { isLoading, loginFetch, loginError, loginSuccess } = useContext(UserContext)

    const { register, handleSubmit, errors } = useForm()

    const onSubmit = handleSubmit((data) => {
        loginFetch({login: 'a@slto.ru', password: '123'})
        // setTimeout(() => {
        //     loginError('Ошыбка!')
        // }, 2000)
        setTimeout(() => {
            loginSuccess('123')
        }, 2000)
    })

    return (
        <div className="card">
            <div className="card__header card__header--center">Вход в аккаунт</div>
            <div className="card__content">
                <form className="form" onSubmit={onSubmit}>
                    <Control id="login-email" name="login" placeholder="Телефон или email" ref={register} />
                    <Control type="password" id="login-password" name="password" placeholder="Пароль" ref={register} />
                    <div className="form__row">
                        <Control type="checkbox" label="Запомнить меня" />
                        <Link to={route('forgot')}>Забыли пароль?</Link>
                    </div>
                    <Button color="main" loading={isLoading} full>Войти</Button>
                    <small>Нужен аккаунт? <Link to={route('register')}>Создать новый</Link></small>
                </form>
            </div>
        </div>
    )
}

export default LoginForm