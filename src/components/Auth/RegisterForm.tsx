import { Link } from "react-router-dom"
import route from "../../hoc/route"
import Button from "../../UI/Button"
import Control from "../../UI/Control"

const RegisterForm = () => {
    return (
        <div className="card">
            <div className="card__header card__header--center">Создание аккаунта</div>
            <div className="card__content">
                <form className="form" action="">
                    <Control id="login-email" name="login-email" placeholder="Телефон или email" />
                    <Control type="password" id="login-password" name="login-email" placeholder="Пароль" />
                    <div className="form__row">
                        <Control type="checkbox" label="Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке," />
                    </div>
                    <Button color="main" full>Войти</Button>
                    <small>Уже есть аккаунт? <Link to={route('login')}>Войти</Link></small>
                </form>
            </div>
        </div>
    )
}

export default RegisterForm