import { useReducer } from "react"
import { STORAGE_KEYS } from "../../utils/constants"
import { LOGIN_ERROR, LOGIN_FETCH, LOGIN_SUCCESS, LOGOUT } from "./types"
import { initialState, UserContext } from "./UserContext"
import { userReducer } from "./UserReducer"


export const UserState = ({children}): JSX.Element => {
    const [state, dispatch] = useReducer(userReducer, initialState)

    const loginFetch = (data) => {
        dispatch({type: LOGIN_FETCH, payload: data})
    }

    const loginError = (error) => {
        dispatch({type: LOGIN_ERROR, payload: error})
    }

    const loginSuccess = (token) => {
        localStorage.setItem(STORAGE_KEYS.TOKEN, token);
        localStorage.setItem(STORAGE_KEYS.IS_LOGGED_IN, 'true');
        dispatch({type: LOGIN_SUCCESS, payload: token})
    }

    const logout = () => {
        localStorage.removeItem(STORAGE_KEYS.TOKEN);
        localStorage.setItem(STORAGE_KEYS.IS_LOGGED_IN, 'false');
        dispatch({type: LOGOUT})
    }

    return (
        <UserContext.Provider value={{
            ...state,
            loginFetch, loginError, loginSuccess, logout
        }}>
            {children}
        </UserContext.Provider>
    )
}