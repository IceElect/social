import { LOGIN_ERROR, LOGIN_FETCH, LOGIN_SUCCESS, LOGOUT } from "./types"

const handlers = {
    [LOGIN_FETCH]: (state, {payload}) => ({...state, isLoading: true}),
    [LOGIN_ERROR]: (state, {payload}) => ({...state, isLoading: false}),
    [LOGIN_SUCCESS]: (state, {payload}) => ({...state, isLoading: false, isLogged: true}),
    [LOGOUT]: (state) => ({...state, isLogged: false}),
    DEFAULT: state => state
}

export const userReducer = (state, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT;
    return handler(state, action)
}