export const LOGIN_FETCH = 'LOGIN_FETCH'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGOUT = 'LOGOUT'

export interface UserState {
    token: string,
    isLogged: boolean,
    isLoading: boolean,
    loginFetch: any,
    loginError: any,
    loginSuccess: any,
    logout: any,
}

interface LoginFetchAction {
    type: typeof LOGIN_FETCH
    payload: {
        login: string,
        password: string
    }
}

interface LoginErrorAction {
    type: typeof LOGIN_ERROR
    payload: string
}

interface LoginSuccessAction {
    type: typeof LOGIN_SUCCESS
    payload: string
}

interface LogoutAction {
    type: typeof LOGOUT
}


export type UserActionTypes = 
    | LoginFetchAction
    | LoginErrorAction
    | LoginSuccessAction
    | LogoutAction