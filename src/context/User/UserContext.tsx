import { createContext } from "react";
import { getIsLoggedIn, getToken } from "../../utils";
import { STORAGE_KEYS } from "../../utils/constants";
import { UserState } from "./types";

export const initialState:UserState = {
    token: getToken(),
    isLogged: getIsLoggedIn(),
    isLoading: false,
    loginFetch: (data) => {},
    loginError: (error) => {},
    loginSuccess: (token) => {},
    logout: () => {},
}

export const UserContext = createContext(initialState)