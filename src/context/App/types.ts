export const SET_TABBAR = 'SET_TABBAR'

export interface AppState {
    tabbar: string,
    setTabbar: Function
}

interface SetTabbarAction {
    type: typeof SET_TABBAR
    payload: string
}


export type AppActionTypes = SetTabbarAction