import { createContext } from "react";

export const initialState = {
    tabbar: "friends",
    setTabbar: (tab: string) => ({})
}

export const AppContext = createContext(initialState);