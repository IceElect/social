import { useReducer } from "react"
import { AppContext, initialState } from "./AppContext"
import { appReducer } from "./AppReducer"
import { SET_TABBAR } from "./types"

export const AppState = ({children}) => {

    const [state, dispatch] = useReducer(appReducer, initialState)

    const setTabbar = (tab: string) => {
        dispatch({type: SET_TABBAR, payload: tab})
    }

    return (
        <AppContext.Provider value={{
            ...state,
            setTabbar
        }}>
            {children}
        </AppContext.Provider>
    )
}