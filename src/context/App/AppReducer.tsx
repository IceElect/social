import { AppActionTypes, AppState, SET_TABBAR } from "./types";

const handlers = {
    [SET_TABBAR]: (state, {payload}) => ({...state, tabbar: payload}),
    DEFAULT: state => state
}

export const appReducer = (state: AppState, action: AppActionTypes) => {
    const handler = handlers[action.type] || handlers.DEFAULT;
    return handler(state, action)
}