import { getIsLoggedIn } from "../../utils";
import route from "../route";

const loggedIn = (to, from, next) => {
    console.log('loggedIn');
    if (getIsLoggedIn()) {
        next();
    }

    next.redirect(route('login'));
};

export default loggedIn