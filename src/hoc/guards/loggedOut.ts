import { getIsLoggedIn } from "../../utils";
import route from "../route";

const loggedOut = (to, from, next) => {
    console.log('loggedOut', getIsLoggedIn())
    if (!getIsLoggedIn()) {
        next();
    }

    next.redirect(route('home'));
};

export default loggedOut