import * as React from "react";
import { Route } from "react-router-dom";
import { UserContext } from "../../context/User/UserContext";
import history from "../history";
import route from "../route";

interface IProps {
  exact?: boolean;
  path: string;
  isAuthenticated: boolean | null;
  component: React.ComponentType<any>;
}

const LoggedOutRoute = ({
    component: Component,
    // isLogged = false,
    ...otherProps
}) => {
    const {isLogged} = React.useContext(UserContext)
    
    if(isLogged === true) {
        history.push(route('home'))
    }

    return (
        <>
            <Route
                render={otherProps => (
                    <>
                        <Component {...otherProps} />
                    </>
                )}
            />
        </>
    )
};

export default LoggedOutRoute;