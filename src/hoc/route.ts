import routes from "./routes";

const route = (name, ...args): string => {

    if(routes[name] === undefined) {
        return ''
    } else {
        return routes[name].path.replaceAll(/(:[a-z]+)+/g, (match) => {
            return args.shift()
        })
        return routes[name].path
            .split(/[\/\@]+/g)
            .map(str => str[0] === ":" ? args.shift() : str)
            .join('/');
    }
}

export default route