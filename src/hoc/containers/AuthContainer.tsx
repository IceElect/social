import { useContext, useEffect } from "react";
import { Route } from "react-router";
import { UserContext } from "../../context/User/UserContext";
import LoginPage from "../../pages/LoginPage";
import RegisterPage from "../../pages/RegisterPage";
import { GuardedRoute } from 'react-router-guards';
import loggedIn from "../guards/loggedIn";
import loggedOut from "../guards/loggedOut";
import route from "../route";

const AuthContainer = (props) => {

    const {isLogged} = useContext(UserContext)
    console.log('AuthContainer');

    return (
        <>
            <div className="page page--auth">
                <GuardedRoute path={route('login')} guards={[loggedOut]} exact component={LoginPage} />
                <GuardedRoute path={route('register')} guards={[loggedOut]} component={RegisterPage} />
            </div>
        </>
    )
}

export default AuthContainer