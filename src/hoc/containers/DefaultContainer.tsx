import { Route } from "react-router";
import Panel from "../../components/Panel";
import Tabbar from "../../components/Tabbar";
import ProfilePage from "../../pages/ProfilePage";
import route from "../route";

const DefaultContainer = (props) => (
    <div className="page">
        <Tabbar />
        <Panel />
        <div className="page__main">
            <Route path={route('profile')} component={ProfilePage} />
        </div>
    </div>
)

export default DefaultContainer