import { Switch, Router, Route } from 'react-router-dom';
import { GuardProvider, GuardedRoute } from 'react-router-guards';
import { AppState } from '../context/App/AppState';
import { UserState } from '../context/User/UserState';
import history from './history';
import PageTitle from './PageTitle';
import routes from './routes';

const Layout = () => {
    
    const providers = [
        AppState,
        UserState
    ]

    return providers.reduce((Acc, CurrentState) => CurrentState({children: Acc}), (
        <Router history={history}>
            <GuardProvider>
                <Switch>
                    {Object.entries(routes).map(([route, {path, container: Cont, middleware: Middleware, exact, title, guards, data}]) => 
                        <GuardedRoute 
                            key={route}
                            exact={exact}
                            path={path} 
                            guards={guards}
                            component={props => (
                                <>
                                    <PageTitle title={title} />
                                    <Cont {...props} data={data} />
                                </>
                            )}
                        />
                    )}
                </Switch>
            </GuardProvider>
        </Router>
    ))
}

export default Layout