import { Route } from "react-router";
import AuthContainer from "./containers/AuthContainer";
import DefaultContainer from "./containers/DefaultContainer";
import loggedIn from "./guards/loggedIn";
import loggedOut from "./guards/loggedOut";
import LoggedInRoute from "./middleware/LoggedInRoute";
import LoggedOutRoute from "./middleware/LoggedOutRoute";

const routes = {
    "home": {
        path: '/',
        container: DefaultContainer,
        title: "Home",
        exact: true,
        middleware: LoggedInRoute,
        guards: [loggedIn],
        meta: {auth: true}
    },
    "login": {
        path: '/login',
        exact: true,
        container: AuthContainer,
        title: "Войти в аккаунт",
        data: test => console.log(test),
        // middleware: LoggedOutRoute,
        guards: [loggedOut],
        // meta: {auth: false}
    },
    "register": {
        path: '/register',
        container: AuthContainer,
        title: "Создать аккаунт",
        data: test => console.log(test),
        // middleware: LoggedOutRoute,
        guards: [loggedOut],
        // meta: {auth: false}
    },
    "profile": {
        path: '/@:id',
        container: DefaultContainer,
        title: "Home",
        exact: true,
        // middleware: LoggedInRoute,
        guards: [loggedIn],
        // meta: {auth: true}
    },
}

export default routes;